urlrouter 7.x-1.x, 2012-03-20
-----------------------------

- rename and package for release

urlrouter 7.x-1.x, 2012-01-31
-----------------------------

- add option for permanent (301) or temporary (302) redirect.

urlrouter 7.x-1.x, 2012-01-25
-----------------------------
jan 25 '13

- proof-of-concept / initial version.
